/** @jsxImportSource theme-ui */

import * as React from "react";
import { Box, Text, Container } from "theme-ui";

const Footer = () => {
  return (
    <Box p={2} bg="background" style={{ height: "4rem" }}>
      <Container sx={{ maxWidth: "700px" }}>
      <Box>
        <Text p={1} sx={{ lineHeight: "2rem" }}>
          © Команда Проєкту «Яр»
        </Text>
      </Box>
    </Container>
    </Box>
  );
};

export default Footer;
