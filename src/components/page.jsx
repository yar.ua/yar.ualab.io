/** @jsxImportSource theme-ui */

import * as React from "react";
import { Flex, Container } from "theme-ui";
import Header from "./header.jsx";
import Footer from "./footer.jsx";

const Page = ({ body }) => {
  return (
    <Flex
      dir="column"
      style={{
        position: "absolute",
        width: "100%",
        flexDirection: "column",
      }}
    >
      <Header />
      <Flex bg="background" p={2} style={{ flexGrow: 1 }}>
        <Container sx={{ maxWidth: "800px" }}>{body}</Container>
      </Flex>
      <Footer />
    </Flex>
  );
};

export default Page;
