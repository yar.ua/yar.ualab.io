/** @jsxImportSource theme-ui */

import * as React from "react";
import { Flex, Box, Link } from "theme-ui";

const Header = () => {
  return (
    <Flex
      bg="greyLighter"
      style={{
        flexWrap: "wrap",
        justifyContent: "right",
        position: "sticky",
        top: 0,
      }}
    >
      <Flex sx={{ lineHeight: ["2rem", "3rem", "4rem"] }}>
        <Box sx={{ padding: "0 1em" }}>
          <Link href="/">Яр</Link>
        </Box>
        <Box sx={{ padding: "0 1em" }}>
          <Link href="/tools">Мовні Інструменти</Link>
        </Box>
        <Box sx={{ padding: "0 1em" }}>
          <Link href="/documentation">Документація</Link>
        </Box>
      </Flex>
      <Box sx={{ flexGrow: "1" }} />
      <Flex sx={{ lineHeight: ["2rem", "3rem", "4rem"] }}>
        <Box sx={{ padding: "0 1em" }}>
          <Link href="https://gitlab.com/yar.ua/">GitLab</Link>
        </Box>
        <Box sx={{ padding: "0 1em" }}>
          <Link href="mailto:team@yar.org.ua">Пошта</Link>
        </Box>
        <Box sx={{ padding: "0 1em" }}>
          <Link href="https://discord.gg/tHYyXR6YzZ">Discord</Link>
        </Box>
        <Box
          sx={{
            color: "primary",
            padding: "0 1rem",
            fontFamily: "icons",
            right: 0,
            fontSize: "1.4rem",
          }}
        >
          A
        </Box>
      </Flex>
    </Flex>
  );
};
export default Header;
