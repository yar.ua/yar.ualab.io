/** @jsxImportSource theme-ui */

import * as React from "react";
import { inflect_cardinal } from "@yar.ua/numerals";
import {
  Flex,
  Box,
  Field,
  Select,
  Label,
  Link,
  Heading,
  Container,
} from "theme-ui";
import Page from "../components/page.jsx";
import { cases } from "./numerals.jsx";

const Inflector = () => {
  const [num, setNum] = React.useState(Math.random().toString().slice(2));
  const [str, setStr] = React.useState("");
  const [error, setError] = React.useState();
  const [form, setForm] = React.useState({
    case: "nominative",
    gender: "masculine",
    number: "singular",
    animacy: "inanimate",
  });
  React.useEffect(() => {
    try {
      setStr(inflect_cardinal(num, form, {}));
      setError();
    } catch (e) {
      setStr("");
      setError(e);
    }
  }, [num, form]);

  return (
    <Box p={1} m={2} bg="background">
      <Flex sx={{flexWrap: "wrap"}}>
        <Field
          name="numeral"
          defaultValue={num}
          m="4pt"
          rootStyle={{ padding: 0 }}
          onChange={(e) => setNum(e.target.value)}
          sx={{ color: error ? "red" : "", padding: 1 }}
        />
        <Select
          m="4pt"
          sx={{ minWidth: 164, padding: 1 }}
          defaultValue={form["case"]}
          onChange={(e) => setForm({ ...form, case: e.target.value })}
        >
          {cases.map(({ value, label }) => (
            <option key={value} value={value}>
              {label}
            </option>
          ))}
        </Select>
      </Flex>
      <Label sx={{ display: "block", paddingTop: 1, fontStyle: "italic" }}>{str}</Label>
    </Box>
  );
};

const Body = () => {
  return (
    <>
      <Heading p={3} sx={{ textAlign: "center" }} as="h1">
        Мовні інструменти
      </Heading>
      <Container bg="muted" p={2}>
        <Heading p={1}>
          <Link href="/numerals">Написання числівників</Link>
        </Heading>
        <text>
          Функціональний інструмент для переведення чисел в текстову форму.
          <br />
          Завершенність: 100%
          <br />
          Доступні імплементації: <Link href="https://www.npmjs.com/package/@yar.ua/numerals">TypeScript</Link>, <Link href="https://pypi.org/project/yar-numerals/">Python</Link>
        </text>
        <Inflector />
        <Link href="/numerals">Повна версія</Link>
      </Container>

      <br />
      <Container bg="muted" p={2}>
        <Heading p={1}>Лексикон</Heading>
        <text>
          База повних парадигм із зазначенням всіх граматичних категорій.
          <br />
          Завершенність: 2%
          <br />
          Покриття: числівники 100%
        </text>
      </Container>

      <br />
      <Container bg="muted" p={2}>
        <Heading p={1}>Корпус текстів</Heading>
        <text>
          База розмічених речень і їх еквівалентних синтаксичних дерев.
          <br />
          Завершенність: 0%
        </text>
      </Container>
      <br />
      <Container bg="muted" p={2}>
        <Heading p={1}>Семантичний граф</Heading>
        <text>
          База знань, граф відношень різноманітних сутностей.
          <br />
          Завершенність: 0%
        </text>
      </Container>
    </>
  );
};

const IndexPage = () => {
  return <Page body={<Body />} />;
};

export default IndexPage;

export const Head = () => <title>Яр. Мовні Інструменти. Українська</title>;
