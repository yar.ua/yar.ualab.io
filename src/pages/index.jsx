/** @jsxImportSource theme-ui */

import * as React from "react";
import { Heading, Container, Paragraph } from "theme-ui";
import Page from "../components/page.jsx";

const Body = () => {
  return (
    <>
      <Heading p={3} sx={{ textAlign: "center" }} as="h1">
        Засади проєкту
      </Heading>
      <Container bg="muted" p={2}>
        <Heading p={1}>Мовні інструменти</Heading>
        <Paragraph variant="block">
          "Яр" – це проєкт, що пропонує сучасні інструменти для машинної
          роботи з українською мовою.
        </Paragraph>
      </Container>
      <br />
      <Container bg="muted" p={2}>
        <Heading p={1}>Відкрита ліцензія</Heading>
        <Paragraph variant="block">
          Усі дані й інструменти мають чітку визначену ліцензію. Ми намагаємось
          створити інструменти, які були б доступні всім, хто хоче використовувати українську в своїх проєктах, незалежно від масштабу і бюджету. Основною ліцензією є MIT-0.
        </Paragraph>
      </Container>
      <br />
      <Container bg="muted" p={2}>
        <Heading p={1}>Докладна документація</Heading>
        <text>
          "Яр" пропонує інструменти тільки для української. Але розробка і
          документація ведеться двома мовами: українською і англійською. 
        </text>
      </Container>
      <br />
      <Container bg="muted" p={2}>
        <Heading p={1}>Сучасна мова</Heading>
        <text>
          Дані й інструменти, які ми росповсюджуємо, орієнтовані на сучасну українську мову.
        </text>
      </Container>
      <br />
      <Container bg="muted" p={2}>
        <Heading p={1}>Безкоштовно</Heading>
        <text>
          Проєкт "Яр" є некомерційним і тримається на зусиллях спільноти і
          пожертвах небайдужих.
        </text>
      </Container>
      <br />
      <Container bg="muted" p={2}>
        <Heading p={1}>Спільнота</Heading>
        <text>
           Ми очікуємо активної участі від усіх, хто користується існуючими інструментами або має потребу в нових. "Яр" завжди відкритий для вашого зворотного зв'язку.	
        </text>
      </Container>
    </>
  );
};

const IndexPage = () => {
  return <Page body={<Body />} />;
};

export default IndexPage;

export const Head = () => <title>Яр. Мовні інструменти. Українська</title>;
