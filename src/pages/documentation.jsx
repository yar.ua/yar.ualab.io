/** @jsxImportSource theme-ui */

import * as React from "react";
import { Box, Heading, Container, Paragraph } from "theme-ui";
import Page from "../components/page.jsx";

const Body = () => {
  return (
    <>
      <Heading p={3} sx={{ textAlign: "center" }} as="h1">
        Документація
      </Heading>

      <Container bg="muted" p={2}>
        <Heading p={1}>Машинно сумісна лінгвістика</Heading>
        <Paragraph variant="block">
          Класична лінгвістика багато років займається описом мовних процесів. І
          ігнорувати її здобутки це розкіш, яку ми не можемо собі дозволити. З
          іншого боку, вживана мова має багато особливостей, які лінгвістика,
          хоч і обгрунтовано, але ігнорує. Чи не найбільшим прикладом є помилки,
          які люди роблять в розмовній мові.
        </Paragraph>
        <br />
        <Paragraph>
          Дуже великий вплив на процес інтеграції лінгвістичних засад в сучасні
          компʼютерні системи має сучасний стан програмних систем. І ці системи,
          на привеликий жаль, не мають змоги підтримувати концепції, з якими
          лінгвісти працюють щодня. Саме тому ми створюємо адаптовану
          лингвістичну систему, яка заснована на сучасних програмних принципах і
          прокладає шлях в світ формального лінгвістичного знання через дещо
          спрощену, але грунтовну "машинно сумісну лінгвістику".
        </Paragraph>
        <br />
        <Paragraph>
          В рамках цього підходу мова обробляється наступними рівнями
          абстракцій:
          <Box p={2}>
            <li>Морфологія</li>
            <li>Граматика</li>
            <li>Лексика</li>
            <li>Синтаксис</li>
            <li>Семантика</li>
          </Box>
        </Paragraph>
      </Container>
    </>
  );
};

const IndexPage = () => {
  return <Page body={<Body />} />;
};

export default IndexPage;

export const Head = () => <title>Яр. Мовні Інструменти. Українська</title>;
