/** @jsxImportSource theme-ui */

import * as React from "react";
import {
  inflect_cardinal,
  inflect_decimal,
  inflect_fraction,
  inflect_ordinal,
} from "@yar.ua/numerals";
import {
  Flex,
  Box,
  Field,
  Container,
  Select,
  Label,
  Radio,
  Switch,
  Heading,
} from "theme-ui";
import Page from "../components/page.jsx";

export const cases = [
  { label: "називний", value: "nominative" },
  { label: "родовий", value: "genitive" },
  { label: "давальний", value: "dative" },
  { label: "знахідний", value: "accusative" },
  { label: "орудний", value: "instrumental" },
  { label: "місцевий", value: "locative" },
  { label: "клична форма", value: "vocative" },
];
const genders = [
  { label: "чоловічий", value: "masculine" },
  { label: "жіночий", value: "feminine" },
  { label: "середній", value: "neuter" },
];

const normalizeValue = (v) => {
  return v.replaceAll(" ", "")
}

const Body = () => {
  const [kind, setKind] = React.useState("cardinal");
  const [value, setValue] = React.useState("");
  const [stress, setStress] = React.useState(false);
  const [color, setColor] = React.useState("main");
  const [stringValue, setStringValue] = React.useState("");
  const [form, setForm] = React.useState({
    case: "nominative",
    gender: "masculine",
    number: "singular",
    animacy: "inanimate",
  });
  React.useEffect(() => {
    if (value === "") {
      setStringValue("")
      setColor("main")
      return
    }
    try {
      if (kind === "cardinal") {
        setStringValue(inflect_cardinal(value, form, { stress }));
      }
      if (kind === "ordinal") {
        setStringValue(inflect_ordinal(value, form, { stress }));
      }
      if (kind === "decimal") {
        const [whole, fraction] = value.split(/[,\\.]/);
        setStringValue(inflect_decimal(whole, fraction, form, { stress }));
      }
      if (kind === "fractional") {
        const [numerator, denominator] = value.split("/");
        setStringValue(
          inflect_fraction("", numerator, denominator, form, { stress })
        );
      }
      setColor("main");
    } catch {
      setStringValue("-");
      setColor("red");
    }
  }, [value, form, stress, kind]);
  return (
      <Box p={4} bg="background" style={{ flexGrow: 1 }}>
        <Heading p={1} sx={{ textAlign: "center" }} as="h1">
          Написання числівників
        </Heading>
        <Container sx={{ flexDirection: "column" }}>
          <Flex p={1} sx={{ alignItems: "flex-start", flexWrap: "wrap", justifyContent: "space-between" }}>
            <Flex
                p={1}
                as="form"
                sx={{flexDirection: "column"}}
                onChange={(e) => setKind(e.target.value)}
            >
              <Label p={1} >
                <Radio
                  name="kind"
                  checked={kind === "cardinal"}
                  value="cardinal"
                />
                Кількісний
              </Label>
              <Label p={1} >
                <Radio name="kind" value="ordinal" /> Порядковий
              </Label>
              <Label p={1} >
                <Radio name="kind" value="decimal" /> Десятковий дріб
              </Label>
              <Label p={1} >
                <Radio name="kind" value="fractional" /> Дріб
              </Label>
            </Flex>

            <Flex sx={{ alignItems: "last baseline", flexDirection: "column" }}>
              <Box p={1} pt={0}>
                <Label p={1}>Відмінок</Label>
                <Select
                  sx={{ minWidth: 164, padding: 1 }}
                  defaultValue={form["case"]}
                  onChange={(e) => setForm({ ...form, case: e.target.value })}
                >
                  {cases.map(({ value, label }) => (
                    <option key={value} value={value}>
                      {label}
                    </option>
                  ))}
                </Select>
              </Box>
              <Box p={1}>
                <Label p={1}>Рід</Label>
                <Select
                  sx={{ width: 164, padding: 1 }}
                  defaultValue={form["gender"]}
                  onChange={(e) => setForm({ ...form, gender: e.target.value })}
                >
                  {genders.map(({ value, label }) => (
                    <option key={value} value={value}>
                      {label}
                    </option>
                  ))}
                </Select>
              </Box>
              </Flex>
            <Flex p={1} sx={{ alignItems: "baseline", flexDirection: "column" }}>
              <Box p={1}>
                <Label>
                  <Switch
                    onChange={(e) =>
                      setForm({
                        ...form,
                        animacy: e.target.checked ? "animate" : "inanimate",
                      })
                    }
                  />
                  Істота
                </Label>
              </Box>
              <Box p={1}>
                <Label>
                  <Switch
                    onChange={(e) =>
                      setForm({
                        ...form,
                        number: e.target.checked ? "plural" : "singular",
                      })
                    }
                  />
                  Множина
                </Label>
              </Box>
              <Box p={1}>
                <Label>
                  <Switch onChange={(e) => setStress(!!e.target.checked)} />
                  Наголос
                </Label>
              </Box>
            </Flex>
          </Flex>

          <Field
              label="Число"
              name="numeral"
              defaultValue=""
              placeholder="0"
              onChange={(e) => setValue(normalizeValue(e.target.value))}
              sx={{ color }}
          />
        </Container>
        <Label
          p={2}
          sx={{ display: "block", fontSize: "x-large", textAlign: "center" }}
        >
          {stringValue}
        </Label>
      </Box>
  );
};

const IndexPage = () => {
  return <Page body={<Body />} />;
};

export default IndexPage;

export const Head = () => <title>Яр. Числівники. Українська</title>;
